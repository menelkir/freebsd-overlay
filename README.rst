FreeBSD Overlay
===============

This is a overlay with a bunch of software not available in FreeBSD ports
in test phases, most of them are are completely broken and/or I'm clueless how to fix it.

All help are welcome. If you want to adopt one of those ports, you're welcome too.

======
Status
======

Ports in directory *Broken* are obviously not working.
Everything else should work as expected.

The ports here will probably never reach official status since commiters change the rules
without saying anything to anyone, so there's no way it depends on anything documented.
Porters handbook isn't updated as it should, if you depend on it, good luck, you'll be
asked for many things that isn't there.

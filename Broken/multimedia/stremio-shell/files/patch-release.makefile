--- release.makefile.orig	2021-05-07 19:53:37 UTC
+++ release.makefile
@@ -1,6 +1,6 @@
 
 BUILD_DIR := build
-INSTALL_DIR := ${PREFIX}/opt/stremio
+INSTALL_DIR := ${PREFIX}share/stremio
 
 ICON_BIN := smartcode-stremio.svg
 

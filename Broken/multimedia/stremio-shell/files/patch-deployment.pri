--- deployment.pri.orig	2021-05-07 19:55:58 UTC
+++ deployment.pri
@@ -3,7 +3,7 @@ unix:!android {
         qnx {
             target.path = $$PREFIX/tmp/$${TARGET}/bin
         } else {
-            target.path = $$PREFIX/opt/$${TARGET}
+            target.path = $$PREFIX/share/$${TARGET}
         }
         export(target.path)
     }
